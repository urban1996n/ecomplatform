<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\Order\Order;
use App\Domain\Model\OrderedProduct\OrderedProduct;
use App\Domain\Model\OrderedProduct\OrderedProductRepositoryInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class OrderedProductRepository
 * @package App\Infrastructure\Repository
 */
final class OrderedProductRepository implements OrderedProductRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * OrderedProductRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(OrderedProduct::class);
    }

   /**
     * @param int $orderedProductId
     * @return Product
     */
    public function findById(int $orderedProductId): ?OrderedProduct
    {
        return $this->objectRepository->find($orderedProductId);
    }

    /**
    * @param Order $order
    * @return array
    */
    public function findByOrder(Order $order): array
    {
        return $this->objectRepository->findAllByOrder($order);        
    }

    /**
     * @param OrderedProduct $orderedProduct
     */
    public function save(OrderedProduct $orderedProduct): void
    {
        $this->entityManager->persist($orderedProduct);
        $this->entityManager->flush();
    }

    /**
     * @param OrderedProduct $orderedProduct
     */
    public function delete(OrderedProduct $orderedProduct): void
    {
        $this->entityManager->remove($orderedProduct);
        $this->entityManager->flush();
    }

}
