<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\Order\Orders;
use App\Domain\Model\Order\OrderRepositoryInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class OrderRepository
 * @package App\Infrastructure\Repository
 */
final class OrderRepository implements OrderRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * OrderRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Orders::class);
    }


    /**
     * @param int $orderId
     * @return Orders
     */
    public function findById(int $orderId): ?Orders
    {
        return $this->objectRepository->find($orderId);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->objectRepository->findAll();
    }

    /**
     * @param Orders $order
     */
    public function save(Orders $order): void
    {
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    /**
     * @param Orders $order
     */
    public function delete(Orders $order): void
    {
        $this->entityManager->remove($order);
        $this->entityManager->flush();
    }

}
