<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\Cart\Cart;
use App\Domain\Model\User\User;
use App\Domain\Model\Cart\CartRepositoryInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CartRepository
 * @package App\Infrastructure\Repository
 */
final class CartRepository implements CartRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * CartRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Cart::class);
    }

     /**
     * @param User $user
     * @return array
     */
    public function findByUser(User $user): ?array
    {
        return $this->objectRepository->findByUser($user);
    }

    /**
     * @param int $cartId
     * @return Cart
     */
    public function findById(int $cartId): ?Cart
    {
        return $this->objectRepository->find($cartId);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->objectRepository->findAll();
    }

    /**
     * @param Cart $cart
     */
    public function save(Cart $cart): void
    {
        $this->entityManager->persist($cart);
        $this->entityManager->flush();
    }

    /**
     * @param Cart $cart
     */
    public function delete(Cart $cart): void
    {
        $this->entityManager->remove($cart);
        $this->entityManager->flush();
    }

}
