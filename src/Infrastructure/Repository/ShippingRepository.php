<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\Shipping\Shipping;
use App\Domain\Model\Shipping\ShippingRepositoryInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ShippingRepository
 * @package App\Infrastructure\Repository
 */
final class ShippingRepository implements ShippingRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * ShippingRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Shipping::class);
    }

   /**
     * @param int $shippingId
     * @return Shipping
     */
    public function findById(int $shippingId): ?Shipping
    {
        return $this->objectRepistory()->find($shippingId);
    }

    /**
     * @param Shipping $shipping
     */
    public function save(Shipping $shipping): void
    {
        $this->entityManager->persist($shipping);
        $this->entityManager->flush();
    }

    /**
     * @param Shipping $shipping
     */
    public function delete(Shipping $shipping): void
    {
        $this->entityManager->remove($shipping);
        $this->entityManager->flush();
    }

}
