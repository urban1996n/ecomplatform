<?php

namespace App\Infrastructure\Http\Rest\Controller;


use App\Application\Service\CartService;
use App\Application\Service\UserService;
use App\Application\Service\ProductService;
use App\Domain\Model\User\User;
use App\Domain\Model\Cart\Cart;
use App\Domain\Model\Cart\CartRepositoryInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class CartController
 * @package App\Infrastructure\Http\Rest\Controller
 */
final class CartController extends FOSRestController
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var User
     */
    private $user;

    /**
     * CartController constructor.
     * @param CartService $cartService
     * @param UserService $userService
     * @param ProductService $productService
     * @param RequestStack $requestStack
     */
    public function __construct(CartService $cartService, UserService $userService, ProductService $productService, RequestStack $requestStack)
    {
        $this->cartService = $cartService;        
        $this->productService = $productService;        
        $request = $requestStack->getCurrentRequest();
        $this->user = $userService->getUserForApiKey($request->get('apiKey'));
    }

    /**
     * Creates an Cart resource
     * @Rest\Post("/carts.{_format}", defaults={"_format"="json"})
     * @param Request $request
     * @return View
     */
    public function postCart(Request $request): View
    {
        $product = $this->productService->getProduct($request->get('productId'));
        $cart = $this->cartService->addCart($this->user, $product, $request->request->get('quantity'));

        // In case our POST was a success we need to return a 201 HTTP CREATED response with the created object
        return View::create($cart, Response::HTTP_CREATED);
    }

    /**
     * Retrieves an Cart resource
     * @Rest\Get("/carts.{_format}", defaults={"_format"="json"})
     * @param int $cartId
     * @return View
     */
    public function getCart(Request $request): View
    {
        $cart = $this->cartService->getCart($this->user);

        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($cart, Response::HTTP_OK);
    }

    /**
     * Replaces Cart resource
     * @Rest\Put("/carts/{cartId}.{_format}", defaults={"_format"="json"})
     * @param int $cartId
     * @param Request $request
     * @return View
     */
    public function putCart(int $cartId, Request $request): View
    {
        $cart = $this->cartService->updateCart($cartId, $request->get('quantity'));

        // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create($cart, Response::HTTP_OK);
    }

    /**
     * Removes the Cart item resource
     * @Rest\Delete("/carts/{cartId}.{_format}", defaults={"_format"="json"})
     * @param int $cartId
     * @return View
     */
    public function deleteCartItem(int $cartId): View
    {
        $this->cartService->deleteCartItem($cartId);

        // In case our DELETE was a success we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create(["message"=>"Cart Item has been deleted"], Response::HTTP_NO_CONTENT);
    }

     /**
     * Removes the Cart resource
     * @Rest\Delete("/carts.{_format}", defaults={"_format"="json"})
     * @param int $cartId
     * @return View
     */
    public function deleteCart(): View
    {
        $this->cartService->deleteCart($this->user);

        // In case our DELETE was a success we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create(["message"=>"Cart has been deleted"], Response::HTTP_NO_CONTENT);
    }
}
