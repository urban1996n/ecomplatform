<?php

namespace App\Infrastructure\Http\Rest\Controller;


use App\Application\Service\ShippingService;
use App\Domain\Model\Shipping\Shipping;
use App\Domain\Model\Shipping\ShippingRepositoryInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class ShippingController
 * @package App\Infrastructure\Http\Rest\Controller
 */
final class ShippingController extends FOSRestController
{
    /**
     * @var ShippingService
     */
    private $shippingService;

    /**
     * ShippingController constructor.
     * @param ShippingService $shippingService
     */
    public function __construct(ShippingService $shippingService)
    {
        $this->shippingService = $shippingService;
    }

    /**
     * Retrieves an Shipping resource
     * @Rest\Get("/shippings/{shippingId}.{_format}", defaults={"_format"="json"})
     * @param int $shippingId
     * @return View
     */
    public function getShipping(int $shippingId): View
    {
        $shipping = $this->shippingService->getShipping($shippingId);


        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($shipping, Response::HTTP_OK);
    }

    /**
     * Retrieves a collection of Shipping resource
     * @Rest\Get("/shippings.{_format}", defaults={"_format"="json"})
     * @return View
     */
    public function getShippings(): View
    {
        $shippings = $this->shippingService->getAllShippings();

        // In case our GET was a success we need to return a 200 HTTP OK response with the collection of shipping object
        return View::create($shippings, Response::HTTP_OK);
    }

    /**
     * Replaces Shipping resource
     * @Rest\Put("/shippings/{shippingId}.{_format}", defaults={"_format"="json"})
     * @param int $shippingId
     * @param Request $request
     * @return View
     */
    public function putShipping(int $shippingId, Request $request): View
    {
        $shipping = $this->shippingService->updateShipping($shippingId, $request->get('client'), $request->get('address'),$request->get('dueDate'),$request->get('type'));

        // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create($shipping, Response::HTTP_OK);
    }
}
