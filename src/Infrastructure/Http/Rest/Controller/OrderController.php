<?php

namespace App\Infrastructure\Http\Rest\Controller;


use App\Application\Service\OrderService;
use App\Application\Service\ShippingService;
use App\Application\Service\CartService;
use App\Application\Service\UserService;
use App\Application\Service\OrderedProductService;
use App\Domain\Model\Order\Order;
use App\Domain\Model\User\User;
use App\Domain\Model\Order\OrderRepositoryInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class OrderController
 * @package App\Infrastructure\Http\Rest\Controller
 */
final class OrderController extends FOSRestController
{
    /**
     * @var OrderService
     */
    private $orderService;
    
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var OrderedProductService
     */
    private $orderedProductService;
    
    /**
     * @var ShippingService
     */
    private $shippingService;

    /**
     * @var User
     */
    private $user;
    
    /**
     * OrderController constructor.
     * @param OrderService $orderService
     * @param CartService $cartService
     * @param OrderedProductService $orderedProductService
     * @param ShippingService $shippingService
     */
    public function __construct(OrderService $orderService,UserService $userService, CartService $cartService, OrderedProductService $orderedProductService, ShippingService $shippingService, RequestStack $requestStack)
    {
        $this->orderService = $orderService;
        $this->cartService = $cartService;
        $this->orderedProductService = $orderedProductService;
        $this->shippingService = $shippingService;
        $request = $requestStack->getCurrentRequest();
        $this->user = $userService->getUserForApiKey($request->get('apiKey'));

    }

    /**
     * Creates an Order resource
     * @Rest\Post("/orders.{_format}", defaults={"_format"="json"})
     * @param Request $request
     * @return View
     */
    public function postOrder(Request $request): View
    {
        $cart = $this->cartService->getCart($this->user);
        $orderedProducts = [];
        $shippingVars = $request->get('shipping');
        $value = 0;
        foreach($cart as $item){
            $value.= ($item->getQuantity() * $item->getProduct()->getPrice());
        }

        $shipping = $this->shippingService->addShipping($shippingVars['client'],$shippingVars['address'],new \DateTime($shippingVars['dueTime']),$shippingVars['type']);
        
        $order = $this->orderService->addOrder($value , new \DateTime(), 'ordered', $this->user, $shipping);
        
        foreach($cart as $item){
            $value.= ($item->getQuantity() * $item->getProduct()->getPrice());
            $orderedProduct = $this->orderedProductService->addOrderedProduct($item->getProduct(),$order,$item->getQuantity());
            $this->cartService->deleteCartItem($item->getId());
            array_push($orderedProducts , $orderedProduct);
        }
        $this->orderService->addOrderedProducts($order->getId(),$orderedProducts);
        // In case our POST was a success we need to return a 201 HTTP CREATED response with the created object
        return View::create($order, Response::HTTP_CREATED);
    }

    /**
     * Retrieves an Order resource
     * @Rest\Get("/orders/{orderId}.{_format}", defaults={"_format"="json"})
     * @param int $orderId
     * @return View
     */
    public function getOrder(int $orderId): View
    {
        $order = $this->orderService->getOrder($orderId);

        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($order, Response::HTTP_CREATED);
    }

    /**
     * Retrieves a collection of Order resource
     * @Rest\Get("/orders.{_format}", defaults={"_format"="json"})
     * @return View
     */
    public function getOrders(): View
    {
        $orders = $this->orderService->getOrders();

        // In case our GET was a success we need to return a 200 HTTP OK response with the collection of order object
        return View::create($orders, Response::HTTP_OK);
    }

    /**
     * Replaces Order resource
     * @Rest\Put("/orders/{orderId}.{_format}", defaults={"_format"="json"})
     * @param int $orderId
     * @param Request $request
     * @return View
     */
    public function putOrder(int $orderId, Request $request): View
    {
        $order = $this->orderService->updateOrder($orderId, $request->get('name'), $request->get('partNumber'),$request->get('price'),$request->get('quantity'));
        // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create($order, Response::HTTP_OK);
    }

    /**
     * Removes the Order resource
     * @Rest\Delete("/orders/{orderId}.{_format}", defaults={"_format"="json"})
     * @param int $orderId
     * @return View
     */
    public function deleteOrder(int $orderId): View
    {
        $this->orderService->deleteOrder($orderId);


        // In case our DELETE was a success we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create(["message"=>"Order has been deleted"], Response::HTTP_NO_CONTENT);
    }
}
