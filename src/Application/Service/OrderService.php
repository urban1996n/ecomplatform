<?php

namespace App\Application\Service;


use App\Domain\Model\Order\Orders;
use App\Domain\Model\OrderedProduct\OrderedProduct;
use App\Domain\Model\User\User;
use App\Domain\Model\Shipping\Shipping;
use App\Domain\Model\Order\OrderRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
/**
 * Class OrderService
 * @package App\Application\Service
 */
final class OrderService
{

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Order constructor.
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository){
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param int $orderId
     * @return Orders
     * @throws EntityNotFoundException
     */
    public function getOrder( int $orderId): Orders
    {
        $order = $this->orderRepository->findById($orderId);
        if (!$order) {
            throw new EntityNotFoundException('Order has not been found');
        }
        
        return $order;
    }

    /**
     * @param int $orderId
     * @return Orders
     * @throws EntityNotFoundException
     */
    public function getOrders(): array
    {
        $order = $this->orderRepository->findAll();
        if (!$order) {
            throw new EntityNotFoundException('Order has not been found');
        }
        return $this->serializer->serialize($order,'json');;
    }

    /**
     * @param Orders $order
     * @param int $value
     * @param \DateTime $orderDate
     * @param string $state
     * @param User $user
     * @param Shipping $shipping
     * @param array $orderedProducts
     * @return Orders
     */
    public function addOrder( int $value, \DateTime $orderDate, string $state, User $user, Shipping $shipping): Orders
    {
        $order = new Orders();
        $order->setOrderValue($value);
        $order->setOrderDate($orderDate);
        $order->setState($state);
        $order->setUser($user);
        $order->setShipping($shipping);
       
        $this->orderRepository->save($order);

        return $this->serializer->serialize($order,'json');;
    }

    /**
     * @param int $orderId
     * @param int $quantity
     * @return Orders
     * @throws EntityNotFoundException
     */
    public function updateOrder(int $orderId, int $value, \DateTime $orderDate, string $state): Orders
    {
        $order = $this->orderRepository->findById($orderId);
        if (!$order) {
            throw new EntityNotFoundException('Order has not been found');            
        }

        $order->setValue($value);
        $order->setOrderDate($orderDate);
        $order->setState($state);
        $this->orderRepository->save($order);

        return $this->serializer->serialize($order,'json');;
    }
    
    /**
     * @param int $orderId
     * @param OrderedProduct $orderedProduct
     * @throws EntityNotFoundException
     */
    public function deleteOrderedProduct(int $orderId, OrderedProduct $orderedProduct): void
    {
        $order = $this->orderRepository->findById($orderId);
        if (!$order) {
            throw new EntityNotFoundException('Order has not been found');            
        }
        $order->removeOrderedProduct($orderedProduct);
        $this->orderRepository->save($order);
    }

    /**
     * @param int $orderId
     * @param array $orderedProducts
     * @throws EntityNotFoundException
     */
    public function addOrderedProducts(int $orderId, array $orderedProducts): void
    {
        $order = $this->orderRepository->findById($orderId);
        if (!$order) {
            throw new EntityNotFoundException('Order has not been found');            
        }
        foreach($orderedProducts as $orderedProduct){
            $order->addOrderedProduct($orderedProduct);
        }
        $this->orderRepository->save($order);
    }

    /**
     * @param int $orderId   
     */
    public function deleteOrder(int $orderId)
    {
        $order = $this->orderRepository->findById($orderId);
        if (!$order) {
            throw new EntityNotFoundException('Order has not been found');            
        }

        $this->orderRepository->delete($orderedProduct);
    }
}
