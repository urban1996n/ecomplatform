<?php

namespace App\Application\Service;


use App\Domain\Model\OrderedProduct\OrderedProduct;
use App\Domain\Model\Product\Product;
use App\Domain\Model\Order\Orders;
use App\Domain\Model\User\User;
use App\Domain\Model\OrderedProduct\OrderedProductRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class OrderedProductService
 * @package App\Application\Service
 */
final class OrderedProductService
{

    /**
     * @var OrderedProductRepositoryInterface
     */
    private $orderedProductRepository;

    /**
     * OrderedProduct constructor.
     * @param OrderedProductRepositoryInterface $orderedProductRepository
     */
    public function __construct(OrderedProductRepositoryInterface $orderedProductRepository){
        $this->orderedProductRepository = $orderedProductRepository;
    }


    /**
     * @param Orders $order
     * @param Product $product
     * @param int $quantity
     * @return OrderedProduct
     */
    public function addOrderedProduct(Product $product, Orders $order, int $quantity): OrderedProduct
    {
        $orderedProduct = new OrderedProduct();
        $orderedProduct->setOrder($order);
        $orderedProduct->setProduct($product);
        $orderedProduct->setQuantity($quantity);
        $this->orderedProductRepository->save($orderedProduct);

        return $orderedProduct;
    }

    /**
     * @param int $orderedProductId
     * @param int $quantity
     * @return OrderedProduct
     * @throws EntityNotFoundException
     */
    public function updateOrderedProduct(int $orderedProductId, int $quantity): OrderedProduct
    {
        $orderedProduct = $this->orderedProductRepository->findById($orderedProductId);
        if (!$orderedProduct) {
            throw new EntityNotFoundException('OrderedProduct has not been found');            
        }

        $orderedProduct->setQuantity($quantity);
        $this->orderedProductRepository->save($orderedProduct);

        return $orderedProduct;
    }

    /**
     * @param int $orderedProductId
     * @throws EntityNotFoundException
     */
    public function deleteOrderedProductItem(int $orderedProductId): void
    {
        $orderedProduct = $this->orderedProductRepository->findById($orderedProductId);
        if (!$orderedProduct) {
            throw new EntityNotFoundException('OrderedProduct has not been found');            
        }

        $this->orderedProductRepository->delete($orderedProduct);
    }
}
