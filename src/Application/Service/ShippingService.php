<?php

namespace App\Application\Service;


use App\Domain\Model\Shipping\Shipping;
use App\Domain\Model\Shipping\ShippingRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class ShippingService
 * @package App\Application\Service
 */
final class ShippingService
{

    /**
     * @var ShippingRepositoryInterface
     */
    private $shippingRepository;

    /**
     * ShippingService constructor.
     * @param ShippingRepositoryInterface $shippingRepository
     */
    public function __construct(ShippingRepositoryInterface $shippingRepository){
        $this->shippingRepository = $shippingRepository;
    }

    /**
     * @param int $shippingId
     * @return Shipping
     * @throws EntityNotFoundException
     */
    public function getShipping(int $shippingId): Shipping
    {
        $shipping = $this->shippingRepository->findById($shippingId);
        if (!$shipping) {
            throw new EntityNotFoundException('Shipping has not been found');
        }
        return $shipping;
    }

    /**
     * @return array|null
     */
    public function getAllShippings(): ?array
    {
        return $this->shippingRepository->findAll();
    }

    /**
     * @param string $client
     * @param string $address
     * @param \DateTime $dueTime
     * @param string $type
     * @return Shipping
     */
    public function addShipping(string $client, string $address,\DateTime $dueTime,string $type): Shipping
    {
        $shipping = new Shipping();
        $shipping->setClient($client);
        $shipping->setAddress($address);
        $shipping->setDueTime($dueTime);
        $shipping->setType($type);
        $this->shippingRepository->save($shipping);

        return $shipping;
    }

    /**
     * @param int $shippingId
     * @param string $client
     * @param string $address
     * @param \DateTime $dueTime
     * @param string $type
     * @return Shipping
     * @throws EntityNotFoundException
     */
    public function updateShipping(int $shippingId, string $client, string $address,\DateTime $dueTime,string $type): Shipping
    {
        $shipping = $this->shippingRepository->findById($shippingId);
        if (!$shipping) {
            throw new EntityNotFoundException('Shipping has not been found');            
        }

        $shipping->setClient($client);
        $shipping->setAddress($address);
        $shipping->setDueTime($dueTime);
        $shipping->setType($type);
        $this->shippingRepository->save($shipping);

        return $shipping;
    }

    /**
     * @param int $shippingId
     * @throws EntityNotFoundException
     */
    public function deleteShipping(int $shippingId): void
    {
        $shipping = $this->shippingRepository->findById($shippingId);
        if (!$shipping) {
            throw new EntityNotFoundException('Shipping has not been found');            
        }

        $this->shippingRepository->delete($shipping);
    }

}
