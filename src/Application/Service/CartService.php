<?php

namespace App\Application\Service;


use App\Domain\Model\Cart\Cart;
use App\Domain\Model\Product\Product;
use App\Domain\Model\User\User;
use App\Domain\Model\Cart\CartRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class CartService
 * @package App\Application\Service
 */
final class CartService
{

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * CartService constructor.
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(CartRepositoryInterface $cartRepository){
        $this->cartRepository = $cartRepository;
    }

    /**
     * @param User $user
     * @return Cart
     * @throws EntityNotFoundException
     */
    public function getCart(User $user): array
    {
        $cart = $this->cartRepository->findByUser($user);
        if (!$cart || empty($cart)) {
            throw new EntityNotFoundException('Cart has not been found');
        }
        return $cart;
    }

    /**
     * @param User $user
     * @param Product $product
     * @param int $quantity
     * @return Cart
     */
    public function addCart(User $user, Product $product, int $quantity): Cart
    {
        $cart = new Cart();
        $cart->setUser($user);
        $cart->setProduct($product);
        $cart->setQuantity($quantity);
        $this->cartRepository->save($cart);

        return $cart;
    }

    /**
     * @param int $cartId
     * @param int $quantity
     * @return Cart
     * @throws EntityNotFoundException
     */
    public function updateCart(int $cartId, int $quantity): Cart
    {
        $cart = $this->cartRepository->findById($cartId);
        if (!$cart) {
            throw new EntityNotFoundException('Cart has not been found');            
        }

        $cart->setQuantity($quantity);
        $this->cartRepository->save($cart);

        return $cart;
    }

    /**
     * @param int $cartId
     * @throws EntityNotFoundException
     */
    public function deleteCartItem(int $cartId): void
    {
        $cart = $this->cartRepository->findById($cartId);
        if (!$cart) {
            throw new EntityNotFoundException('Cart has not been found');            
        }

        $this->cartRepository->delete($cart);
    }

    /**
     * @param User $user
     */
    public function deleteCart(User $user){
        $cart = $this->getCart($user);
        foreach($cart as $item){
            $this->cartRepository->delete($item);
        }
    }
}
