<?php

namespace App\Application\Service;


use App\Domain\Model\Product\Product;
use App\Domain\Model\Product\ProductRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class ProductService
 * @package App\Application\Service
 */
final class ProductService
{

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * ProductService constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository){
        $this->productRepository = $productRepository;
    }

    /**
     * @param int $productId
     * @return Product
     * @throws EntityNotFoundException
     */
    public function getProduct(int $productId): Product
    {
        $product = $this->productRepository->findById($productId);
        if (!$product) {
            throw new EntityNotFoundException('Product has not been found');
        }
        return $product;
    }

    /**
     *@param string $name  
     *@return array||null
     * 
     */
    public function getProducts(string $name): ?array
    {
        return $this->productRepository->findByName($name);
    }
    
    /**
     * @return array||null
     */
    public function getAllProducts(): ?array
    {
        return $this->productRepository->findAll();
    }

    /**
     * @param string $name
     * @param string $partNumber
     * @param int $price
     * @param int $quantity
     * @return Product
     */
    public function addProduct(string $name, string $partNumber,int $price, int $quantity): Product
    {
        $product = new Product();
        $product->setName($name);
        $product->setPartNumber($partNumber);
        $product->setPrice($price);
        $product->setQuantity($quantity);
        $this->productRepository->save($product);

        return $product;
    }

    /**
     * @param int $productId
     * @param string $name
     * @param string $partNumber
     * @param int $price
     * @param int $quantity
     * @return Product
     * @throws EntityNotFoundException
     */
    public function updateProduct(int $productId, string $name, string $partNumber,int $price, int $quantity): Product
    {
        $product = $this->productRepository->findById($productId);
        if (!$product) {
            throw new EntityNotFoundException('Product has not been found');            
        }

        $product->setName($name);
        $product->setPartNumber($partNumber);
        $product->setPrice($price);
        $product->setQuantity($quantity);
        $this->productRepository->save($product);

        return $product;
    }

    /**
     * @param int $productId
     * @throws EntityNotFoundException
     */
    public function deleteProduct(int $productId): void
    {
        $product = $this->productRepository->findById($productId);
        if (!$product) {
            throw new EntityNotFoundException('Product has not been found');            
        }

        $this->productRepository->delete($product);
    }

}
