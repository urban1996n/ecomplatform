<?php

namespace App\Domain\Model\OrderedProduct;
use App\Domain\Model\Order\Order;
/**
 * Interface OrderedProductRepositoryInterface
 * @package App\Domain\Model\OrderedProduct
 */
interface OrderedProductRepositoryInterface
{
  
   /**
     * @param int $orderedProductId
     * @return Product
     */
    public function findById(int $orderedProductId): ?OrderedProduct;
    
   /**
    * @param Order $order
    * @return array
    */
    public function findByOrder(Order $order): array;
    
   /**
     * @param OrderedProduct $orderedProduct
     */
    public function save(OrderedProduct $orderedProduct): void;

    /**
     * @param OrderedProduct $orderedProduct
     */
    public function delete(OrderedProduct $orderedProduct): void;

}
