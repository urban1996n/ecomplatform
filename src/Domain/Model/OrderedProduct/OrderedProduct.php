<?php

namespace App\Domain\Model\OrderedProduct;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class OrderedProduct
 * @ORM\Entity
 * @package App\Domain\Model\OrderedProduct
 */
class OrderedProduct
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Product\Product", inversedBy="orderedProducts")
     */
    private $product;
   
    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Order\Orders", inversedBy="orderedProducts")
     */
    private $order;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $quantity;

     /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return App\Domain\Model\Product\Product
     */
    public function getProduct(): \App\Domain\Model\Product\Product
    {
        return $this->product;
    }

    /**
     * @param App\Domain\Model\Product\Product $product
     */
    public function setProduct(\App\Domain\Model\Product\Product $product): void
    {        
        $this->product = $product;
    }

    /**
    * @return App\Domain\Model\Order\Orders
     */
    public function getOrder(): \App\Domain\Model\Order\Orders
    {
        return $this->order;
    }

    /**
     * @param App\Domain\Model\Order\Orders $product
     */
    public function setOrder(\App\Domain\Model\Order\Orders $order): void
    {        
        $this->order = $order;
    }
    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {        
        $this->quantity = $quantity;
    }
}
