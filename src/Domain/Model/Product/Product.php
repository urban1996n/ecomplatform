<?php

namespace App\Domain\Model\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Product
 * @ORM\Entity
 * @package App\Domain\Model\Product
 */
class Product
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $partNumber;
    
    /**
     * @ORM\Column(type="float")
     * @var float
     */
    private $price;
   
    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $quantity;

     /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @throws \InvalidArgumentException
     */
    public function setName(string $name): void
    {
        if (\strlen($name) < 5) {
            throw new \InvalidArgumentException('Name needs to have more then 5 characters.');
        }
        
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPartNumber(): string
    {
        return $this->partNumber;
    }

    /**
     * @param string $partNumber
     */
    public function setPartNumber(string $partNumber): void
    {
        $this->partNumber = $partNumber;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {        
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {        
        $this->quantity = $quantity;
    }
}
