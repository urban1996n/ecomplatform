<?php

namespace App\Domain\Model\Order;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation\MaxDepth;
/**
 * Class Orders
 * @ORM\Entity
 * @package App\Domain\Model\Orders
 */
class Orders
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;
    
    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $orderValue;

     /**
     * @ORM\Column(type="datetime")
     * @var datetime
     */
    private $orderDate;

     /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $state;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @ORM\OneToOne(targetEntity="App\Domain\Model\Shipping\Shipping")
     * @ORM\JoinColumn(name="shipping_id", referencedColumnName="id")
     */
    private $shipping;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Model\OrderedProduct\OrderedProduct", mappedBy="order")
     */
    private $orderedProducts;

    //Initializing array collection of ordered products.    
    public function __construct()
    {
        $this->orderedProducts = new ArrayCollection();
    }

     /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
    
    /**
     * @return int
     */
    public function getOrderValue(): int
    {
        return $this->orderValue;
    }

    /**
     * @param int $orderValue
     */
    public function setOrderValue(int $orderValue): void
    {        
        $this->orderValue = $orderValue;
    }

    /**
     * @return \DateTime
     */
    public function getOrderDate(): \DateTime
    {
        return $this->orderDate;
    }

    /**
     * @param \DateTime $orderDate
     */
    public function setOrderDate(\DateTime $orderDate): void
    {        
        $this->orderDate = $orderDate;
    }
    
    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param \DateTime $orderDate
     */
    public function setState(string $state): void
    {        
        $this->state = $state;
    }

    /**
     * @return App\Domain\Model\User\User
     */
    public function getUser(): \App\Domain\Model\User\User
    {
        return $this->user;
    }

    /**
     * @param App\Domain\Model\User\User $user
     */
    public function setUser(\App\Domain\Model\User\User $user): void
    {        
        $this->user = $user;
    }

    /**
     * @return App\Domain\Model\Shipping\Shipping
     */
    public function getShipping(): \App\Domain\Model\Shipping\Shipping
    {
        return $this->shipping;
    }

    /**
     * @param App\Domain\Model\User\User $user
     */
    public function setShipping(\App\Domain\Model\Shipping\Shipping $shipping): void
    {        
        $this->shipping = $shipping;
    }

    /*
    * @return Collection|OrderedProducts[]
    */
    public function getOrderedProducts(): Collection
    {
       return $this->orderedProducts;
    }

     /**
     * @param App\Domain\Model\OrderedProduct\OrderedProduct $orderedProduct
     * @return Ordered
     */
    public function addOrderedProduct(\App\Domain\Model\OrderedProduct\OrderedProduct $orderedProduct): Orders
    {
        if (!$this->orderedProducts->contains($orderedProduct)) {
            $this->orderedProducts[] = $orderedProduct;
            $orderedProduct->setOrder($this);
        }

        return $this;
    }
    
     /**
     * @param App\Domain\Model\OrderedProduct\OrderedProduct $orderedProduct
     * @return Ordered
     */
    public function removeOrderedProduct(\App\Domain\Model\OrderedProduct\OrderedProduct $orderedProduct): Orders
    {
        if ($this->orderedProducts->contains($product)) {
            $this->orderedProducts->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($orderedProduct->getOrder() === $this) {
                $orderedProduct->setOrder(null);
            }
        }

        return $this;
    }
}
