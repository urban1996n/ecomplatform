<?php

namespace App\Domain\Model\Order;

/**
 * Interface OrdersRepositoryInterface
 * @package App\Domain\Model\Orders
 */
interface OrderRepositoryInterface
{

    /**
     * @param int $orderId
     * @return Orders
     */
    public function findById(int $orderId): ?Orders;

    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @param Orders $order
     */
    public function save(Orders $order): void;

    /**
     * @param Orders $order
     */
    public function delete(Orders $order): void;

}
