<?php

namespace App\Domain\Model\Cart;
use App\Domain\Model\User\User;

/**
 * Interface CartRepositoryInterface
 * @package App\Domain\Model\Cart
 */
interface CartRepositoryInterface
{

    /**
     * @param User $user
     * @return array
     */
    public function findByUser(User $user): ?array;
    
    /**
     * @param  $cartId
     * @return Cart
     */
    public function findById(int $cartId): ?Cart;

    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @param Cart $cart
     */
    public function save(Cart $cart): void;

    /**
     * @param Cart $cart
     */
    public function delete(Cart $cart): void;

}
