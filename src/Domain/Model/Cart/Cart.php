<?php

namespace App\Domain\Model\Cart;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Cart
 * @ORM\Entity
 * @package App\Domain\Model\Cart
 */
class Cart
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\Product\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;
   
    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Model\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
   
    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $quantity;

     /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return App\Domain\Model\Product\Product
     */
    public function getProduct(): \App\Domain\Model\Product\Product
    {
        return $this->product;
    }

    /**
     * @param App\Domain\Model\Product\Product $product
     */
    public function setProduct(\App\Domain\Model\Product\Product $product): void
    {        
        $this->product = $product;
    }
     
    /**
     * @return App\Domain\Model\User\User
     */
    public function getUser(): \App\Domain\Model\User\User
    {
        return $this->user;
    }

    /**
     * @param App\Domain\Model\User\User $user
     */
    public function setUser(\App\Domain\Model\User\User $user): void
    {        
        $this->user = $user;
    }
     
    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {        
        $this->quantity = $quantity;
    }
}
