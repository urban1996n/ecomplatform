<?php

namespace App\Domain\Model\Shipping;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Shipping
 * @ORM\Entity
 * @package App\Domain\Model\Shipping
 */
class Shipping
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $client;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $address;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $dueTime;
   
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $type;

     /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getClient(): string
    {
        return $this->client;
    }

    /**
     * @param string $client
     * @throws \InvalidArgumentException
     */
    public function setClient(string $client): void
    {
        if (\strlen($client) < 5) {
            throw new \InvalidArgumentException('Client needs to have more then 5 characters.');
        }
        
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return \DateTime
     */
    public function getDueTime(): \DateTime
    {
        return $this->dueTime;
    }

    /**
     * @param \DateTime $dueTime
     */
    public function setDueTime(\DateTime $dueTime): void
    {        
        $this->dueTime = $dueTime;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {        
        $this->type = $type;
    }
}
