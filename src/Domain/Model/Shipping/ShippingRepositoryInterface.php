<?php

namespace App\Domain\Model\Shipping;

/**
 * Interface ShippingRepositoryInterface
 * @package App\Domain\Model\Shipping
 */
interface ShippingRepositoryInterface
{

    /**
     * @param Shipping $shipping
     */
    public function save(Shipping $shipping): void;

    /**
     * @param Shipping $shipping
     */
    public function delete(Shipping $shipping): void;

}
